#! coding=utf-8
from reportlab.platypus import PageTemplate, Frame
from reportlab.lib.units import cm


class LogBookPage(PageTemplate):
    def __init__(self, page_size, title, registration, date, *args, **kwargs):
        self.page_size = page_size
        self.title = title
        self.date = date
        self.registration = registration
        self.page_height = self.page_size[1]
        self.page_width = self.page_size[0]
        self.log_frame = Frame(1 * cm, 1.5 * cm, self.page_width - (2 * cm), self.page_height - (3.5 * cm), id='TableFrame', showBoundary=0)
        super(LogBookPage, self).__init__(id='LogBookPage', frames=[self.log_frame])

    def beforeDrawPage(self, canvas, doc):
        canvas.saveState()
        canvas.setFont('Times-Bold', 16)
        canvas.drawString(1*cm, self.page_height - (1.5*cm), self.title)
        canvas.drawCentredString(self.page_width / 2.0, self.page_height - (1.5*cm), self.date)
        canvas.drawRightString(self.page_width - (1*cm), self.page_height - (1.5*cm), self.registration)
        canvas.restoreState()

        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        canvas.drawString(1 * cm, (1 * cm), 'Not for Official Use')
        canvas.drawCentredString(self.page_width / 2.0, (1 * cm), "Page {}".format(doc.page))
        canvas.drawRightString(self.page_width - (1 * cm), (1 * cm), "Not for Official Use")
        canvas.restoreState()
