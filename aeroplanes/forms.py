from datetimewidget.widgets import DateWidget
from django.forms import ModelForm
from .models import Aeroplane


class AeroplaneForm(ModelForm):
    class Meta:
        model = Aeroplane
        fields =\
            [
                'arc_expiry',
                'radio_expiry',
                'insurance_expiry'
            ]

        widgets = {
            "arc_expiry": DateWidget(attrs={'id': 'arc_expiry_id'}, usel10n=True, bootstrap_version=3),
            "radio_expiry": DateWidget(attrs={'id': 'radio_expiry_id'}, usel10n=True, bootstrap_version=3),
            "insurance_expiry": DateWidget(attrs={'id': 'insurance_expiry_id'}, usel10n=True, bootstrap_version=3),
        }
