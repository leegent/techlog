from django.forms import widgets, inlineformset_factory, ModelForm

from group.models import GroupProfile, GroupMemberProfile


class GroupConsumableRateForm(ModelForm):
    class Meta:
        model = GroupProfile
        fields = [
                "current_fuel_rebate_price_per_litre",
                "current_oil_rebate_price_per_litre",
                "default_cost_per_unit",
        ]
        widgets = {
            "current_fuel_rebate_price_per_litre": widgets.NumberInput(attrs={'class': 'form-control'}),
            "current_oil_rebate_price_per_litre": widgets.NumberInput(attrs={'class': 'form-control'}),
            "default_cost_per_unit": widgets.NumberInput(attrs={'class': 'form-control'})
        }


InlineMemberRateForm = inlineformset_factory(
    GroupProfile,
    GroupMemberProfile,
    fields=(
        'current_cost_per_unit',
    ),
    widgets=
    {
        "current_cost_per_unit": widgets.NumberInput(attrs={'class': 'form-control'})
    },
    extra=0,
    can_delete=False
)
