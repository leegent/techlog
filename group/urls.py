from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.group, name="group"),
    url(r'^rates/(?P<pk>\d+)/$', views.group_rates, name="group_rates"),
    url(r'^set-group-rates/(?P<pk>\d+)/$', views.set_group_rates, name="set_group_rates"),
    url(r'^set-member-rates/(?P<pk>\d+)/$', views.set_member_rates, name="set_member_rates"),
    url(r'^create/$', views.creategroup, name="creategroup"),
    url(r'^join/(?P<secret>[\w\d-]+)/$', views.join_group, name="joingroup"),

]
