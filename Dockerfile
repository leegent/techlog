FROM python:3.6
ENV PYTHONBUFFERED=1 \
    DATABASE_URL=postgres://user:pass@dbhost:5432/db \
    TECHLOG_STATIC_USE_S3=False \
    TECHLOG_MEDIA_USE_S3=False \
    TECHLOG_DEBUG=False \
    TECHLOG_ALLOWED_HOSTS="techlog.com techlog.net" \
    PORT=8000

WORKDIR /app
ADD requirements.txt .
RUN pip install -r requirements.txt
ADD . .
RUN python manage.py collectstatic --noinput
CMD ["/bin/sh", "-c", "python manage.py migrate --noinput && gunicorn -w 4 -b 0.0.0.0:$PORT techlog.wsgi" ]
EXPOSE $PORT
